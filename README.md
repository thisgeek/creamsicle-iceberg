High-contrast highlights for iceberg.vim's support for [terminal emulators](https://github.com/cocopon/iceberg.vim#terminal-emulators).

Doesn't yet support beta releases of iTerm2, as in <https://github.com/cocopon/iceberg.vim/pull/105>.
